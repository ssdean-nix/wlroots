{
  description = "Wlroots flake";

  # Flake inputs
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  # Flake outputs
  outputs = { self, nixpkgs }: let
    # Systems supported
    allSystems = ["x86_64-linux" "x86_64-darwin" ];

    # Helper to provide system-specific attributes
    forAllSystems = f: nixpkgs.lib.genAttrs allSystems (system: f {
      pkgs = import nixpkgs { inherit system; };
    });

    in {
      packages = forAllSystems ({ pkgs }: {
        default = with pkgs; stdenv.mkDerivation rec {
          pname = "wlroots";
          version = "master";
          name = "${pname}-${version}";
          src = pkgs.fetchgit {
            url = "https://gitlab.freedesktop.org/wlroots/wlroots.git";
            rev = "dbc7a5cada37d192821e1104b356ff891b8bd787";
            sha256 = "sha256-+V9i7Yv1qezGJSO2OW7hcrPf8ueg1NXXxZxR39xuLU4=";
          };

          nativeBuildInputs = [ glslang meson ninja pkg-config wayland-scanner ];
          buildInputs = [
            cmake
            clang
            ffmpeg_4
            hwdata
            libdisplay-info
            libGL
            libcap
            libinput
            libliftoff
            libpng
            libxkbcommon
            mesa
            pixman
            seatd
            vulkan-loader
            wayland
            wayland-protocols
            xorg.libX11
            xorg.xcbutilerrors
            xorg.xcbutilimage
            xorg.xcbutilrenderutil
            xorg.xcbutilwm
            xwayland
          ];
        };
      });
    };
}
